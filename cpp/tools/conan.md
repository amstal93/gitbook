# Conan

Conan est un gestionnaire de paquets pour le C++. Il permet donc de gérer plus simplement les dépendances d'un projet.

## Consommer un paquet

### Conanfile.txt
Pour consommer un paquet, on peut utiliser des fichier *conanfile.txt* écris au format *ini*. On peut retrouver la syntaxe complète dans la [documentation officielle](https://docs.conan.io/en/latest/reference/conanfile_txt.html) dont cet page est tirée.

{% hint style="info" %}
Il est également possible d'utiliser un [fichier `conanfile.py`](https://docs.conan.io/en/1.36/mastering/conanfile_py.html). La syntaxe de ce fichier permet d'utiliser du python et donc de faire plus de configurations. 
{% endhint %}


Il suffit d'indiquer les dépendances nécessaires au projet avec leur version dans la section `requires`.

```txt
[requires]
box2d/2.4.1
```

On peut également spécifié une plage de version (ou seulement un minimum).

```txt
[requires]
nlohmann_json/[>3.0,<3.1.9]
```

Si des éléments doivent être utilisés pour compiler il faudra les indiquer dans les `build_requires`. On mettra ici des outils et non des bibliothèques auxquels votre projet doit *linker*. Cette section supporte également l'utilisation de plage de version.

```txt
[build_requires]
7zip/16.00
```


Ensuite il faut indiquer quel type de configuration générer. Cela permet d'intégrer facilement ces bibliothèques dans un projet. La liste des générateurs est disponible [ici](https://docs.conan.io/en/latest/reference/generators.html#generators-reference).
On peut sélectionner autant de générateur que l'on veut.

```txt
[generators]
cmake
```

On peut également donner des [options](https://docs.conan.io/en/latest/using_packages/conanfile_txt.html#options-txt) pour chacun des paquets. Les options peuvent être différentes pour chaque paquet.

```txt
[options]
box2d:shared=True
```

Enfin on peut spécifier des [éléments à importer](https://docs.conan.io/en/latest/using_packages/conanfile_txt.html#imports-txt). Ces éléments seront copiés dans le dossier de build de votre projet. On peut s'en servir pour rassembler les bibliothèques dynamiques ou des licenses par exemple. Dans l'exemple suivant on copie tous les fichiers dans le dossier lib des paquets vers le dossier bin dans le dossier de compilation.

```txt
[imports]
lib, * -> ./bin
```

Enfin on peut ajouter des commentaires en les commençant par `#`. Les commentaires ne sont terminés que lors du retour à la ligne.

```txt
# Commentaire
```

### Installation des dépendances

Une fois le fichier configuré, il suffit d'utiliser la commande `conan install`.
On peut y spécifier une multitude d'options généralement liées à la machine sur laquelle la compilation à lieu.



### Consommer directement depuis CMake

Il existe une [intégration officielle](https://github.com/conan-io/cmake-conan) de Conan dans CMake. Il suffit de télécharger le module et de l'inclure dans le projet CMake. Il est possible de télécharger ce fichier directement depuis CMake. L'exemple dans le `README.md` montre comment faire cette intégration.

```cmake
if(NOT EXISTS "${CMAKE_BINARY_DIR}/conan.cmake")
  message(STATUS "Downloading conan.cmake from https://github.com/conan-io/cmake-conan")
  file(DOWNLOAD "https://raw.githubusercontent.com/conan-io/cmake-conan/v0.16.1/conan.cmake"
                "${CMAKE_BINARY_DIR}/conan.cmake"
                EXPECTED_HASH SHA256=396e16d0f5eabdc6a14afddbcfff62a54a7ee75c6da23f32f7a31bc85db23484
                TLS_VERIFY ON)
endif()

include(${CMAKE_BINARY_DIR}/conan.cmake)
```

{% hint style="warning" %}
Le module est parfois mis à jour, il faut alors changer l'URL de téléchargement pour profiter des derniers ajouts et corrections.
{% endhint %}

On peut ensuite intégrer les paquets que l'on veut directement depuis CMake.

```cmake
conan_cmake_run(
    REQUIRES nlohmann_json/3.9.1
             catch2/2.13.6
    BASIC_SETUP CMAKE_TARGETS
    BUILD missing
)
```

Toutes les options de `conan install` sont disponibles depuis cette commande.

{% hint style="warning" %}
Le `README.md` n'est (aujourd'hui) pas à jour. Il faut suivre l'exemple présent [ici](https://github.com/conan-io/cmake-conan#conan_cmake_run-options).
{% endhint %}

L'utilisation de ce module, oblige à ajouter une option lors de la génération avec CMake. Les options possibles sont disponibles dans la [documentation de CMake](https://cmake.org/cmake/help/latest/variable/CMAKE_BUILD_TYPE.html).

```bash
cmake . -DCMAKE_BUILD_TYPE=Release
```

## Créer un paquet

Pour créer un paquet on utilise un `conanfile.py`. Votre paquet sera alors décrit dans une *class* python héritant de `ConanFile` dont certaines méthodes permettent de définir des comportements spécifiques.

Tout d'abord on donne la classe et quelques informations sous la forme de variable de classe. Une multitute d'autres attributs sont disponibles, la [documentation officielle](https://docs.conan.io/en/latest/reference/conanfile.html) les décrits tous.

```py
from conans import ConanFile, CMake
from conans import tools

class MyConanPkg(ConanFile):
  name = "MyconanPkg"
  version = "1.0.0"
  settings = "os", "compiler", "build_type", "arch" # Contient les infos sur la plateforme compilant le paquet
```

Il faut ensuite décrire comment obtenir le code source nécessaire à la création du paquet. De cette manière, votre paquet sera compilable sur les plateformes dont vous ne proposez pas directement des binaires précompilés.

```py
def source(self):
  self.run("git clone <url>") # Git n'est pas nécessaire, vous pouvez utiliser n'importe quelle commande de votre système d'exploitation (attention au multiplateforme).
```

Ensuite il faut indiquer comment compiler le projet.

```py
def build(self): # Utilise CMake pour l'exemple mais peut utiliser n'importe quelle commande du système.
  cmake = CMake(self) # On profite de l'intégration CMake dans python.
  self.run(f"cmake . {cmake.command_line}")
  self.run(f"cmake --build . {cmake.build_config}")
```

{% hint style="info" %}
Différents [outils](https://docs.conan.io/en/1.36/reference/conanfile/tools.html) permettent de simplifier l'utilisation de systèmes de compilations.
{% endhint %}

Maintenant on sélectionne les fichiers à intégrer dans le paquet. Les paramètres de copies sont : 

1. Expression régulière permettant de trouver les fichiers à copier
2. Dossier dans lequel chercher les fichiers à copier (relatif au dossier de compilation)
3. Emplacement vers lequel copier les fichiers

```py
def package(self):
  self.copy("*", "include", "SFML/include")
  self.copy("*.lib", "lib", "SFML/lib", keep_path=False)
  self.copy("*.so", "lib", "SFML/lib", keep_path=False)
  self.copy("*.dll", "lib", "SFML/lib", keep_path=False)
```

Et enfin on donne l'information sur les bibliothèques pouvant être obtenues par ce paquet.

```py
def package_info(self):
  self.cpp_info.libdirs = ["lib", "bin"] # Liste des dossiers contenant des bibliothèques (contient lib par défaut).
  self.cpp_info.libs.append(f"mylib") # Plusieurs bibliothèques peuvent être ajoutés en faisant plusieurs `append`.
```

Ce fichier permet également d'obtenir des dépendances nécessaires à la création du paquet. Il faura configurer les variables de classe pour indiquer les dépendances et leur configuration, la [documentation officielle](https://docs.conan.io/en/1.36/mastering/conanfile_py.html) donne un bon exemple.
