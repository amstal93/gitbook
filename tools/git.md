# Git

Git est un gestionnaire de versions, c'est à dire qu'il permet de stocker un fichier au moment que l'on désire, et de rebasculer sur une version antérieure.

Au début du projet, on initialise git. Pour cela, on se place depuis le terminal à la racine de notre projet puis on initialise ce qu'on va appeler notre "repository", c'est à dire un dépôt de fichiers suivis par git.

```bash
git init
```

Une fois la commande effectuée, un dossier *.git* apparaît à la racine de votre projet. C'est dans ce dossier que seront stockée les informations sur le *repository*. En le 
supprimant votre projet perd toute information sur git.

## Fonctionnement des versions

On appel *commit* le code sauvegardé à un instant précis. Git ne nous permet que de passer de commit en commit, il ne sauvegarde pas toutes les modifications intermédiaires. C'est pourquoi il faut faire des commits le plus régulièrement possible.

{% hint style="info" %}
En réalité git ne stocke pas chaque version du fichier mais plutôt les différences entre le commit précédent et le commit actuel
{% endhint %}

Pour faire un commit il faut plusieurs commandes. La première permet d'ajouter des fichiers au commit. On ne peut y ajouter que des fichiers qui ont été modifié, créé ou supprimer depuis le commit précédent.

```bash
git add . # Ajout de tous les fichiers et dossiers du répertoire courant à git. (Seulement ceux qui ont changé)
```

{% hint style="info" %}
Les dossiers ne sont pas réellement ajouté dans git. Ce qui est retenu est l'emplacement de chaque fichier. Donc un dossier vide n'existe pas, et l'ajout d'un dossier ajoute en réalité les fichiers qu'il contient.
{% endhint %}

On demande ensuite à git de stocker cette version du code. Le paramètre `-m` permet d'ajouter un message décrivant les changements effectués.

```bash
git commit -m "Initialisation du projet"
```

On peut ensuite commencé à développer. Et à chaque avancée du code, on peut créer un nouveau point dans git.
Imaginons que j'ai écrit le code nécessaire à la réalisation d'un premier test unitaire, je dois refaire un **commit**.

```bash
git add .
git commit -m "Passage du premier test unitaire"
```

On peut continuer ainsi et créer un commit pour chaque étape.
Si on doit revenir sur le commit précédent avec git on peut utiliser git reset avec l'option `--hard`.

```bash
git reset --hard HEAD~1 # 1 correspond au nombre de commits que l'on veut annuler
```

{% hint style="warn" %}
Attention il ne sera plus possible de revenir sur un commit futur après avoir fait le reset.
{% endhint %}

## Organisation des commits

Plutôt que d'avoir un seul historique de commit, git permet de créer des branches.

Pour créer une branche il y a deux solutions.

```bash
git branch <nouvelle branche> # Créer seulement la branche.
git checkout -b <nouvelle branche> # Déplace dans la nouvelle branche après l'avoir crée.
```

La deuxieme commande permet également de se déplacer entre les branches.

```bash
git checkout <branche>
```

{% hint style="info" %}
Il n'est pas possible de changer de branche, sans avoir enregistré les changements actuels.
Vous devez donc faire un commit, annuler les changements ou bien utiliser les *stashs*.
{% endhint %}

Pour rappatrié des changements d'une branche vers une autre, il faut faire un *merge*. Lorsque l'on fait une fusion, le commit dans lequel est fait la fusion est **commun** aux deux branches. 

```bash
git merge <branche source>
```

Si dans les deux branches, des commits modifient la même portion de code, il faudra alors faire un "resolve". On devra indiquer à Git, si l'on préfère garder le code actuel, celui qui arrive ou bien autre chose. L'action de merge créant un commit, il est possible d'y faire les modifications que l'on souhaite.

Lors d'un resolve les conflits apparaissent de la manière suivante :

```
<<<<<<< HEAD
this is some content to mess with
content to append
=======
totally different content to merge later
>>>>>>> new_branch_to_merge_later
```

Le HEAD montre le contenu actuel qui est séparé du contenu arrivant par une ligne de *=*.

## Partage d'un repository

Git est un gestionnaire de version décentralisé, chacun possède sa propre copie du dépôt. On peut donc intéragir avec les autres. Pour cela on va créer un repository *distant*. Il sera hébergé sur un serveur. Vous pouvez en héberger vous même ou utiliser des services tels que [Gitlab](https://www.gitlab.com), [Github](https://www.github.com) ou [Bitbucket](https://bitbucket.org).

La méthode pour créer le dépôt distant dépend de chaque site, je ne la couvrirai donc pas ici. Je pars du principe que le dépôt créé ne sera pas initialisé.

### Initialisation de la relation avec le dépôt distant

Pour initialiser le dépôt distant, on doit simplement rattacher le dépot local au distant.

```bash
git remote add <nom> <url> # Le nom généralement utilisé est "origin"
```

Il faut ensuite lier la branche actuelle à une branche distante. Si cette dernière n'existe pas elle sera créée.

```bash
git branch --set-upstream-to=<nom du dépôt distant>/<nom de la branche distante> # Ce qui donne généralement "origin/<nom de la branche locale>"
```

### Echange de commits avec le dépôt distant

Pour publier ses commits sur la branche distante, il faut faire un **push**.

```bash
git push
```

Pour récupérer les changements de la branche distante, on fait un **pull**.

```bash
git pull
```

Il arrive que votre dépôt local ne sache pas que des modifications ont eut lieu à distance. Il faudra alors *fetch* les informations avant de pouvoir pull.

```bash
git fetch
```

### Initialisation d'un dépôt local à partir d'un dépôt distant

Si le dépôt a déjà était créé, il suffit de le *cloner* localement.

```bash
git clone <url>
```

Un nouveau dossier sera créé portant le nom du dépôt. Il contiendra le repository.

{% hint style="info" %}
Vous pouvez ajouter à la fin de la commande un dossier, pour indiquer où vous préférez que le contenu du dépôt soit téléchargé. Attention ce dossier doit-être vide.
{% endhint %}
