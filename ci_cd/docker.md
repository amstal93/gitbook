# Docker

Docker est un outil permettant la création de conteneurs. Il ne faut pas confondre conteneur et machine virutelle. Dans le cadre d'un conteneur, seule la couche logicielle est séparée. C'est à dire que le système d'exploitation ne change pas. Alors que pour une VM l'entiereté de l'OS et de ce qu'il contient va être encapsulé. La VM est donc plus sécurisée mais aussi plus lourde.

Pour résumer Docker fonctionne comme ça :

![Schéma montrant le fonctionnement des conteneurs docker](./docker.svg)

Et une machine virtuelle c'est comme ça :

![Schéma montrant le fonctionnement des machines virtuelles](./vm.svg)

Ces diagrammes sont repris du site [circleci.com](https://circleci.com/blog/virtual-machines-vs-docker-containers-a-comparison/?utm_medium=SEM&utm_source=gnb&utm_campaign=SEM-gb-DSA-Eng-emea&utm_content=&utm_term=dynamicSearch-&gclid=Cj0KCQiA_qD_BRDiARIsANjZ2LAI6b6Kg4V3kJACHkq97fOOVvRR_zazRrvT9bxZ9AJ73ps_8DbKiv8aAhy_EALw_wcB).

## Utilisation simple de Docker

Dans un premier temps il va falloir lancer un conteneur. Chaque conteneur est basé sur une image. Une multitude d'images existe et est disponnible depuis le site [Docker Hub](https://hub.docker.com/) (aucun compte requis pour utiliser les images).

On va dans un premier temps utiliser un Ubuntu pour les exemples. La première commande permet de créer un conteneur à partir d'une image (Ubuntu disponnible sur Docker Hub). Normalement l'image n'étant pas disponnible en local, docker commencera par la télécharger. Enfin le conteneur sera créé.
```bash
docker run ubuntu
```

Attention, qu'un conteneur soit créé ne signifie pas qu'il est en cours de fonctionnement. Il est possible de voir les conteneurs existants ainsi que les informations le concernant. L'argument `-a` permet de voir également les conteneurs qui ne sont pas en cours de fonctionnement.
```bash
docker ps -a
```

Docker génère par défaut un nom et un identifiant pour les conteneurs. On s'intéresse surtout au nom qui nous permet de plus facilement identifier les conteneurs. Le noms par défaut étant aléatoires, on préfère spécifier le nom des conteneurs lorsque l'on travaille avec. Il n'est pas possible de modifier ni l'identifiant ni le nom d'un conteneur.
On va donc supprimer le conteneur et en créer un autre dont le nom a été spécifié.
```bash
docker rm <nom_du_conteneur> # Fonctionne également avec l'identifiant.
docker run --name mon_ubuntu ubuntu
```

On peut vérifier que le nom est bon avec la commande utilisée précédemment :
```bash
docker ps -a
```

Supprimons maintenant toutes les images existantes, attention cette commande peut être dangereuse... L'argument `-q` de `docker ps` permet de n'afficher que les identifiants des conteneurs. 
```bash
docker rm $(docker ps -a -q)
```

### Spécification de la version de l'image
On a dans un premier temps utilisé l'image "ubuntu" or il existe plusieurs versions d'ubuntu. Docker permet de spécifier une version de l'image. On utilise alors les "tags".
La page [Docker Hub d'Ubuntu](https://hub.docker.com/_/ubuntu?tab=description&page=1&ordering=last_updated) indique les tags disponnibles pour l'image ubuntu.

Le tag par défaut est toujours "latest" ce qui correspond pour Ubuntu (au moment où la page est écrite) à la version 20.04 du système d'exploitation. Nous allons maintenant utilisé la version 20.10 qui est plus récente. Les quatres commandes ont le même comportement, il est possible de donner plusieurs tags à une même version d'une image, c'est une sorte d'alias.

```bash
docker run --name mon_ubuntu_20_10 ubuntu:20.10
docker run --name mon_ubuntu_20_10 groovy-20201125.2
docker run --name mon_ubuntu_20_10 groovy
docker run --name mon_ubuntu_20_10 rolling
```

On peut de nouveau supprimer les conteneurs existants.

### Utiliser un conteneur

Les conteneurs créé jusqu'à maintenanet n'avaient aucun but. Si on veut exécuter une commande directement depuis le conteneur on peut le specifier à la création. La commande `nproc` sera exécutée depuis le conteneur. On aurait également pu exécuter une commande avec des paramêtres telle que `echo "Hello World"`. 
```bash
docker run --name mon_bash_ubuntu ubuntu:20.10 nproc
```

L'inconvénient de cette méthode est qu'il faut détruire manuellement le conteneur avant de pouvoir exécuter à nouveau une commande dans ce conteneur. Pour éviter ça on peut lancer un `bash` en mode intéractif.
```bash
docker run -it --name mon_bash_ubuntu ubuntu:20.10 bash
```
On a maintenant accès à un terminal `bash` dans le système de l'image docker, on peut donc utiliser toutes les commandes que l'on souhaite. (Petit rappel, pour quitter un `bash` il suffit de faire `exit`)

{% hint style="info" %}
Pour supprimer automatiquement un conteneur lorsqu'il devient inactif on utilise l'option `--rm` de `docker run`.
```bash
docker run --rm ubuntu:20.10 nproc
```
{% endhint %}

## Accès avec l'extérieur du conteneur

Le conteneur ne peut pas accéder ni aux éléments de l'hôte ni au réseau. Il est cependant possible de spécifier lors de la création du conteneur ce à quoi il peut accéder. On va utiliser pour l'exemple une image docker contenent un serveur web disponnible comme toujours sur [Docker Hub](https://hub.docker.com/r/danjellz/http-server). Cette image expose le dossier `/public` sur le port `8080`. 

### Réseau

Docker permet de facilement relier un port du conteneur à un port de la machine hôte. On utilise l'argument `--publish` (ou `-p`) de la commande `docker run`.
```bash
docker run -it --rm -p 8080:8080 danjellz/http-server
```
En accédant à l'url : [localhost:8080](localhost:8080) depuis le navigateur internet, on remarque que l'on accède correctement au serveur web.

Pour rendre le serveur web plus sécurisé, on préfère le rediriger sur un port extérieur différent (ici le 34876).
```bash
docker run -it --rm -p 34876:8080 danjellz/http-server
```

### Dossiers
Il faut maintenant rendre accessibles un dossier de l'hôte au conteneur. Pour cela il existe plusieurs méthodes : les `mount` et les `volume`.

Les `mount` sont plus complexes à utiliser, mais ils permettent plusieurs choses :
- dossiers partagés en réseau (NAS) avec les identifiants et protocoles à utiliser
- gestion des droits (lecture seule)
- sûrement plus
Le détail du fonctionnement de cette méthode est ici : [docs.docker.com](https://docs.docker.com/storage/volumes/).

On va ici utiliser les volumes qui sont plus simples. Le fonctionnement est le même que pour les ports : `-v <dossier_hote>:<dossier_conteneur>`.
```bash
docker run -it --rm -p 34876:8080 -v $(PWD):/public danjellz/http-server
```

{% hint style="danger" %}
Sous Windows il faut remplacer `$(PWD)` par `${PWD}`.
{% endhint %}
